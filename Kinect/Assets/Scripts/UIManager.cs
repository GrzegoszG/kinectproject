﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    //public RectTransform rect;

    //public List<RawImage> images;

    public Slider slider;
    public Scrollbar scrollbar;
    public ScrollRect rect;
    public float offset;
    private bool exit = false;
    private IEnumerator coroutine;

    public float timeToStopCoroutines;
    private float timeLeft;
    void Awake()
    {
        coroutine = SetValue(0);
       // images = rect.gameObject.GetComponentsInChildren<RawImage>().ToList();
    }

    private void Update()
    {
        if(exit)
        {
            if(coroutine != null)
            {
                StopCoroutine(coroutine);
                exit = false;
            }
        }
        if(timeLeft < 0)
        {
            StopCoroutine(coroutine);
            StopAllCoroutines();
            timeLeft = timeToStopCoroutines;
        }
        if(timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
    }

    private bool isRotating = false;

    private int rotatingDirection = 0;
    
    public void Rotate(int direction)
    {
        Debug.ClearDeveloperConsole();
        Debug.Log(direction);
        if (direction == 0)
            return;

        if (direction < 0)
        {
            RotateLeft();
        }
        else
        {
            RotateRight();
        }
    }

    private float epsilon = 0.01f;
    public float speed;

    IEnumerator SetValue(float value)
    {
        string hehe = isRotating ? " NOT POSSIBLE" : " OK";
        
        if (isRotating) yield return null;

        isRotating = true;

        float direction = value > scrollbar.value ? 1 : -1;

        float startPos = scrollbar.value;
        float newPosition = 0;
        float diff = Mathf.Abs(value);
        float increment = 0;
        Debug.Log("New Coroutine" + hehe + "\tDiff : " +diff + "\tDirection : " + direction);
        while (true)
        {
            yield return new WaitForEndOfFrame();
            increment = (speed * Time.deltaTime * direction);
            newPosition = scrollbar.value + increment;
            if (newPosition >= 0 && newPosition <= 1)
            {
                scrollbar.value = newPosition;
                if(direction > 0)
                {
                    if (newPosition >= value)
                        break;
                }
                else
                {
                    if (newPosition <= value)
                        break;
                }
                diff = Mathf.Abs(scrollbar.value - increment);
            }
            else
                break;

            //newPosition = rect.horizontalNormalizedPosition + (speed * Time.deltaTime * direction);
            //if (newPosition >= 0 && newPosition <= 1)
            //    rect.horizontalNormalizedPosition = newPosition;
            //else
            //    break;
            //            Debug.Log(rect.horizontalNormalizedPosition);

        }
        double temp = scrollbar.value;
        temp = System.Math.Round(temp, 2);
        scrollbar.value = (float)temp;
        isRotating = false;
        exit = true;
        yield return null;
    }

    private void RotateRight()
    {
        //Debug.Log("RIGHT");
        float newValue = Mathf.Clamp01(scrollbar.value) + offset;
        newValue = Mathf.Clamp01(newValue);
        if(scrollbar.value != newValue && newValue < 0.93)
            StartRotating(newValue);
    }

    private void RotateLeft()
    {
        //Debug.Log("LEFT");
        float newValue = Mathf.Clamp01(scrollbar.value) - offset;
        newValue = Mathf.Clamp01(newValue);
        if (scrollbar.value != newValue)
            StartRotating(newValue);
    }

    private void StartRotating(float value)
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = SetValue(value);
        StartCoroutine(coroutine);
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
