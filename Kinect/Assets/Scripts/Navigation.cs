﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigation : MonoBehaviour
{
    public List<Transform> Path1;
    public List<Transform> Path2;
    public List<Transform> Path3;
    
    public static Navigation Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            return;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
            Instance = this;
        else
            return;
    }
    
    public List<Transform> Path(int index)
    {
        if (index == 1)
        {
            return Path1;
        }
        else if (index == 2)
        {
            return Path2;
        }
        else if (index == 3)
        {
            return Path3;
        }
        else
            return null;
    }

}
