﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoStreamer : MonoBehaviour
{
    public VideoPlayer vp;
    public RawImage RawImage;

    private VideoClip VideoClip;

    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }
    
    public void Activate()
    {
        StartCoroutine(PlayVideo());
    }

    IEnumerator PlayVideo()
    {
        vp.Prepare();
        WaitForSeconds wait = new WaitForSeconds(1);
        while(!vp.isPrepared)
        {
            yield return wait;
            break;
        }
        RawImage.texture = vp.texture;
        vp.Play();
    }
}
