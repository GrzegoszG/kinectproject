﻿using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform Parent;
    public Vector3 rotation;
    public float Smooth;

    public bool UseMovingAvg = false;
    public int MovingAvgSize;

    private Quaternion newQ;

    public List<Vector3> q = new List<Vector3>();

    public float calibrationTime = 2f;

    private bool calibrate;

    private float calTimeLeft = 0;

    private void Start()
    {
        calibrate = true;
        calTimeLeft = calibrationTime;
        avgSize = MovingAvgSize;
        MovingAvgSize = 60;
        offset = transform.rotation.eulerAngles;
    }

    private int avgSize = 0;
    private Vector3 offset;

    // Update is called once per frame
    void Update()
    {
        rotation = UDP.Rotation;
        rotation = new Vector3(-rotation.y, rotation.z, rotation.x);
        transform.position = Parent.position;
        newQ = UseMovingAvg ? Average(rotation) : Quaternion.Euler(rotation);
        
        if(calibrate)
        {
            if(calTimeLeft <= 0)
            {
                offset += AverageV(q[q.Count - 1]);
                MovingAvgSize = avgSize;
                calibrate = false;
            }
            else
            {
                calTimeLeft -= Time.deltaTime;
            }
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, newQ, Smooth * Time.deltaTime);
        }
    }

    private Quaternion Average(Vector3 rot)
    {
        rot -= offset;
        return Quaternion.Euler(AverageV(rot));
    }

    private Vector3 AverageV(Vector3 rot)
    {
        Vector3 avg = new Vector3();

        if (q.Count >= MovingAvgSize)
        {
            q.RemoveAt(0);
        }
        if (q.Count < MovingAvgSize)
        {
            q.Add(rot);
        }
        for (int i = 0; i < q.Count; i++)
        {
            avg += q[i];
        }
        avg.x /= (float)q.Count;
        avg.y /= (float)q.Count;
        avg.z /= (float)q.Count;

        return avg;
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log("Player Hit " + other.gameObject.name + " WITH TAG : " + other.tag);
        if (other.CompareTag("Enemy"))
        {
            var e = other.gameObject.GetComponent<Enemy>();
            if (e != null)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit))
                    e.GetHit(hit);
                else
                    e.GetHit();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Player Hit " + other.gameObject.name + " WITH TAG : "+other.tag);
        if(other.CompareTag("Enemy"))
        {
            var e = other.gameObject.GetComponent<Enemy>();
            if (e != null)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit))
                    e.GetHit(hit);
                else
                    e.GetHit();
            }
                
        }
    }
}
