﻿using UnityEngine;

public class Configuration : MonoBehaviour
{
    [SerializeField]    
    private float timeBtwSpawnEnemy = 5;
    public static float TimeBtwSpawnEnemy
    {
        get { return Instance.timeBtwSpawnEnemy; }
    }

    [SerializeField]
    private int enemyHp = 1;
    public static int EnemyHP
    {
        get { return Instance.enemyHp; }
    }

    [SerializeField]
    private int playerHP = 5;
    public static int PlayerHP
    {
        get { return Instance.playerHP; }
    }

    [SerializeField]
    private bool oneEnemy = true;
    public static bool OneEnemy
    {
        get { return Instance.oneEnemy; }
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            return;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
            instance = this;
        else
            return;
    }

    private Configuration()
    {

    }

    private static Configuration instance = null;
    public static Configuration Instance
    {
        get
        {
            return instance;
        }
    }
}
