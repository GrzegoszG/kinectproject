﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KulkaSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform Kulka;
    public Transform StartPos;
    public float MaxDistance;

    void Start()
    {
        Kulka = transform.Find("Sphere");
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(Kulka.position, StartPos.position) > MaxDistance)
        {
            Kulka.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Kulka.position = StartPos.position;
        }
    }
}
