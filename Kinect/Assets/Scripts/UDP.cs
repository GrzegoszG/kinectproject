﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class UDP : MonoBehaviour
{

    public int Port = 5555;

    private static string message = "";
    public static string Message => message;

    private UdpClient Client;

    private static Vector3 rotation;
    public static Vector3 Rotation => rotation;

    private Thread thread;
    public bool Connected;
    public Vector3 ReceivedRotation;
    public string ReceivedMessage;

    public bool TryConnect;
    private static bool tryConnect;

    // Start is called before the first frame update
    void Start()
    {
        TryConnect = true;
        tryConnect = true;
        rotation = new Vector3();
        thread = new Thread(Listen);
        thread.Start();
        Debug.Log(thread.IsAlive);
        
    }

    private void Update()
    {
        Connected = Client?.Client?.Connected ?? false;
        ReceivedRotation = Rotation;
        ReceivedMessage = Message;
        tryConnect = TryConnect;
        //if(BytesReceived > 0)
        //Debug.Log(BytesReceived);
    }

    private void OnDisable()
    {
        if (thread.IsAlive)
            thread.Abort();
        Client.Close();
    }

    public static int BytesReceived = 0;

    private void Listen()
    {
        Client = new UdpClient(Port);
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 8000);
        while (tryConnect)
        {
            Receive(RemoteIpEndPoint);
            message = Client.Client.Connected.ToString();

        }
    }

    private float rotX, rotY, rotZ;

    private Vector3 ProcessBytes(byte[] bytes)
    {
        List<byte> tempList = new List<byte>();
        tempList = bytes.ToList();
        tempList.Reverse();
        bytes = tempList.ToArray();
        float[] rot = new float[3] { 0, 0, 0 };

        using (var stream = new MemoryStream(bytes))
        {
            using (var reader = new BinaryReader(stream))
            {

                stream.Seek(0, SeekOrigin.Begin);

                rotX = (float)Math.Round(reader.ReadSingle());
                rotY = (float)Math.Round(reader.ReadSingle());
                rotZ = (float)Math.Round(reader.ReadSingle());
                rotation.Set(rotX, rotY, rotZ);
            }
        }

        /*
        for (int i = 0; i < 3; i++)
        {
            var temp = BitConverter.ToSingle(bytes, i * 3);
            rot[i] = (float)Math.Round(temp, 2);
        }
        */
        return rotation;
    }

    //CallBack

    private void Receive(IPEndPoint RemoteIpEndPoint)
    {
        byte[] received = Client.Receive(ref RemoteIpEndPoint);
        
        var list = received.ToList();
        BytesReceived += list.Count();
        int startindex = list.FindIndex(b => b > 0);
        if (startindex > 0)
        {
            list.RemoveRange(0, startindex);
            received = list.Take(12).ToArray();
            rotation = ProcessBytes(received);
        }
    }

    private void recv(IAsyncResult res)
    {
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 8000);
        byte[] received = Client.EndReceive(res, ref RemoteIpEndPoint);
        var list = received.ToList();
        int startindex = list.FindIndex(b => b > 0);
        if (startindex > 0)
        {
            list.RemoveRange(0, startindex);
            received = list.Take(12).ToArray();
            var processed = ProcessBytes(received);
        }
        Client.BeginReceive(new AsyncCallback(recv), null);
    }

}
