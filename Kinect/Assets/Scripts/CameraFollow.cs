﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform mainCamera;
    public Transform toLook;
    // Start is called before the first frame update
    
    // Update is called once per frame
    void LateUpdate()
    {
        mainCamera.transform.position = transform.position;
        mainCamera.transform.LookAt(toLook);
    }
}
