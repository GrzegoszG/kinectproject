﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    int a = 0;
    private GameMaster gm;
    public GameObject panel;
    public Text scoreText;

    private void Start()
    {
        gm = GetComponent<GameMaster>();
        OnPauseChanged(false);
    }

    public void Print()
    {
        Debug.Log(a);
    }

    public void OnPauseChanged(bool isPaused)
    {
        if(isPaused)
        {
            panel.SetActive(true);
        }
        else
        {
            panel.SetActive(false);
        }
    }

    public void ButtonResumeClick()
    {
        gm.SetPause(false);
    }

    public void ButtonExitClick()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void SetPoints(int score)
    {
        scoreText.text = "SCORE : " + score;
    }

    public static void Cos()
    {
        Debug.Log("Naciskniento!!!");
    }
}
