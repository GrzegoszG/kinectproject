﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float Speed;
    public GameObject WeaponSlot;
    private Animator animator;
    //private Animation animation;
    private AnimationClip [] clips;
    private Transform start;
    public List<GameObject> weaponPrefabs;
    public int HP;
    public int HPMax;
    private int pathIndex = -3;
    public int PathIndex => pathIndex;
    public GameObject splashPrefab;

    private List<AnimationClip> attackAnims;

    private GameObject weapon;
    private Transform weaponEnd;

    private List<Transform> path;
    private bool run;

    public GameObject attackTarget;
    private Rigidbody rb;

    public float timeBtwHits;
    private float timeHitLeft;

    private static string weaponChildPath = "PT_Medieval_Hips/PT_Medieval_Spine/PT_Medieval_Spine2/PT_Medieval_Spine3/PT_Medieval_RightShoulder/PT_Medieval_RightArm/PT_Medieval_RightForeArm/PT_Medieval_RightHand/PT_Medieval_Right_Hand_Weapon_slot";
    
    // Start is called before the first frame update
    void Start()
    {
        timeHitLeft = timeBtwHits;
        HPMax = Configuration.EnemyHP;
        HP = HPMax;

        rb = GetComponent<Rigidbody>();
        rb.Sleep();
        animator = GetComponent<Animator>();
        //animation = GetComponent<Animation>();
        clips = animator.runtimeAnimatorController.animationClips;
        attackAnims = clips.Where(c => c.name.ToLower().Contains("attack") && !c.name.Contains("360")).ToList();
        attackTarget = GameObject.Find("PLAYER");
        animator.StopPlayback();
        animator.enabled = false;
        //animator.Play("standing_block_idle", 0);
        //InitPath();
        Equip();
        
        animator.enabled = true;
        animator.Play("standing_run_forward_inPlace", 0);
        run = true;
    }

    private void Equip()
    {
        WeaponSlot = transform.Find(weaponChildPath).gameObject;
        int rand = Random.Range(0, weaponPrefabs.Count);
        weapon = Instantiate(weaponPrefabs[rand], WeaponSlot.transform);
        weaponEnd = weapon.GetComponentInChildren<Transform>();
    }

    private Transform posBeforeAttack;
    private Transform currentTarget;

    public void SetPath(int _pathIndex)
    {
        pathIndex = _pathIndex;
        path = Navigation.Instance.Path(_pathIndex);
        start = path[0];
        currentTarget = path[1];
        transform.position = start.position;
        transform.LookAt(currentTarget);
    }

    private float epsilon = 0.6f;

    private bool checkRun = true;
    private bool attack = false;
    public string clipMessage = "";

    // Update is called once per frame
    void Update()
    {
        if (timeHitLeft >= 0)
            timeHitLeft -= Time.deltaTime;

        var info = animator.GetNextAnimatorClipInfo(0);
        if(info.Any())
            currentClip = info[0].clip;
        clipMessage = currentClip?.name ?? "";
        if (checkRun)
        {
            if (currentTarget == null)
                return;

            if (Vector3.Distance(transform.position, currentTarget.position) < epsilon)
            {
                SetNextTarget();
            }
            if (run)
            {
                transform.position = Vector3.MoveTowards(transform.position, currentTarget.position, Speed * Time.deltaTime);
            }
            else
            {
                checkRun = false;
                transform.LookAt(attackTarget.transform);
                animator.StopPlayback();
                attack = true;
            }
        }
        else if(attack)
        {
            Attack();
        }
    }

    private AnimationClip currentClip;

    private AnimationClip GetRandomAttack()
    {
        int rand = Random.Range(0, attackAnims.Count);
        return attackAnims[rand];
    }

    private float animTime0;

    private void Attack()
    {
        if(animTime0 <= 0)
        {
            var newAttack = GetRandomAttack();
            animTime0 = newAttack.length;
            transform.LookAt(attackTarget.transform);
            animator.Play(newAttack.name);
        }
        else
        {
            animTime0 -= Time.deltaTime;
        }
    }

    private void SetNextTarget()
    {
        var index = path.IndexOf(currentTarget);
        if (index < path.Count - 1)
        {
            currentTarget = path[index + 1];
            transform.LookAt(currentTarget);
        }
        else
        {
            currentTarget = transform;
            run = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log("Enemy hit by " + other.gameObject.name);
        //Debug.Log("Enemy hit by " + other.gameObject.name);
        if (other.gameObject.name.Equals("playerWeapon"))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
                GetHit(hit);
            else
                GetHit();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Enemy hit by " + other.gameObject.name);
        //Debug.Log("Enemy hit by " + other.gameObject.name);
        if (other.gameObject.name.Equals("playerWeapon"))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
                GetHit(hit);
            else
                GetHit();
        }
    }

    private bool isHit = false;
    public void GetHit(RaycastHit? hit = null)
    {
        if (isHit || timeHitLeft > 0)
            return;
        isHit = true;
        
        if(hit != null)
        {
            var temp = Instantiate(splashPrefab, hit.Value.transform.position, Quaternion.identity);
            temp.GetComponent<ParticleSystem>().Play();
            GameObject.Destroy(temp, 1);
        }

        timeHitLeft = timeBtwHits;
        HP--;

        if (HP <= 0)
        {
            Die();
        }
        isHit = false;
    }

    private void Die()
    {
        GameMaster.Instance.HandleEnemyDie(this);
    }
}
