﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public List<GameObject> enemyPrefabs;
    public List<Enemy> Enemies;
    public int Score;

    public static GameMaster Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this, 0.01f);
        }
        Enemies = new List<Enemy>();
    }

    private void Start()
    {
        TimeToSpawn = Configuration.TimeBtwSpawnEnemy;
        SetPause(false);
    }

    public void SpawnEnemy()
    {
        if(enemyPrefabs.Count == 0 || (Configuration.OneEnemy && Enemies.Any()))
        {
            return;
        }
        int path = GetNewPath();
        if(path < 1)
        {
            return;
        }

        int rand = Random.Range(0, enemyPrefabs.Count);
        var prefab = enemyPrefabs[rand];

        var temp = Instantiate(prefab, Navigation.Instance.Path(path)[0]);
        var enemy = temp.GetComponent<Enemy>();
        enemy.SetPath(path);
        Enemies.Add(enemy);
        totalEnemies++;
    }

    private int totalEnemies = 0;

    private int GetNewPath()
    {
        List<int> list = new List<int>(new []{1, 2, 3});
        int index = -2;

        index = Configuration.OneEnemy ? 1 : list[Random.Range(0, list.Count)];

        if(index == 1)
        {
            if (CheckPathSpawn(index))
                return index;

            index = 2;
            if (CheckPathSpawn(index))
                return index;

            index = 3;
            if (CheckPathSpawn(index))
                return index;
        }
        else if(index == 2)
        {
            if (CheckPathSpawn(index))
                return index;

            index = 3;
            if (CheckPathSpawn(index))
                return index;

            index = 1;
            if (CheckPathSpawn(index))
                return index;
        }
        else if(index == 3)
        {
            if (CheckPathSpawn(index))
                return index;

            index = 1;
            if (CheckPathSpawn(index))
                return index;

            index = 2;
            if (CheckPathSpawn(index))
                return index;
        }
        index = -2;
        return index;
    }

    public bool IsPaused = false;

    public void SetPause(bool? value = null)
    {
        if (value.HasValue)
        {
            if (value.Value == IsPaused)
            {
                return;
            }
            IsPaused = value.Value;
        }
        else
        {
            IsPaused = !IsPaused;
        }

        Time.timeScale = IsPaused ? 0 : 1;
        var guiController = GetComponent<GUIController>();
        guiController.OnPauseChanged(IsPaused);

        Debug.Log("PAUSE");
    }

    private bool CheckPathSpawn(int pathIndex)
    {
        //string check = "Checking Path [ " + pathIndex + "] : ";
        //check += string.Join(", ", Enemies.Select(e => "Occupied index " + e.PathIndex).ToList());
        var onGivenIndex = Enemies.FirstOrDefault(e => e.PathIndex == pathIndex);
        bool free = onGivenIndex == null;
        if (free)
        {
          //  check += " - "+ pathIndex +" is FREE!";
        }
        //Debug.Log(check);
        return pathIndex >= 1 && pathIndex <=3 && free;
    }

    public float TimeToSpawn;

    private void Update()
    {
        if(Enemies.Count < 3)
        {
            if (TimeToSpawn <= 0)
            {
                SpawnEnemy();
                TimeToSpawn = Configuration.TimeBtwSpawnEnemy;
            }
            else
            {
                TimeToSpawn -= Time.deltaTime;
            }
        }
    }

    private bool deleting = false;
    public void HandleEnemyDie(Enemy enemy)
    {
        if (deleting) return;
        deleting = true;
        enemy.enabled = false;
        Enemies.Remove(enemy);
        enemy.gameObject.transform.position.Set(0, -5, 0);
        Destroy(enemy.gameObject, 1f);
        Score = totalEnemies - 1;
        var gui = GetComponent<GUIController>();
        gui.SetPoints(Score);
        deleting = false;
    }
}

